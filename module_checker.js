$(document).ready(function() {
	var divHead = $('#module-checker-head');
	var divMain = $('#module-checker-main');

	divHead.html ('Requesting projects list...');

	//Load projects list first
	$.getJSON ('/admin/build/modules/module_checker/projects', {},
		function( data ) {
			//generate list
			var strHtml = '';
			for (var proj in data) {
				strHtml += '<div class="row" id="row-'+ proj +'"><span class="project-check" mod="'+ data[proj][0] +'" proj="'+ proj +'">'+ proj +'</span>';
				strHtml += '<div class="row-info"></div></div>';
			}
			divMain.html (strHtml);
			//add status informer
			divHead.html ('Up to date: <span id="module-checker-uptodate" cnt="0">...</span> of <span id="module-checker-total">...</span>. To be updated <span id="module-checker-toupdate" cnt="0">...</span>.');
			//start checking
			_mc_project_has_updates (divMain, 0);
		}
	);
});

function _mc_project_has_updates (divMain, ind) {
	var projects = [];
	var modules  = [];

	divMain.find ('.project-check').each (function(){
		projects.push ($(this).attr ('proj'));
		modules.push  ($(this).attr ('mod'));
	});
//alert (projects.length +'<='+ ind);
	$('#module-checker-total').html (projects.length);
	if (projects.length <= ind) {
		return;
	}

	//current project to check
	var proj = projects[ind];
	var mod  = modules [ind];
	//mark as checking one
	$('.project-check[proj='+ proj +']').addClass ('mod-checking');
	//request data
	$.getJSON ('/admin/build/modules/module_checker/status', 
		{project: proj, module: mod},
		function ( data ) {
			if (data['result'] == 'ok') {
				var current     = data['data']['current'];				
				var currentDate = parseInt (current['date']);
				var suggest = [];
				//check what's new
				for (var v in data['data']) {
					var verion = v;
					var date   = data['data'][v][0];
					var changes= data['data'][v][1];

					if (parseInt (date) > currentDate + 10)  {
						suggest.push ([v, changes, date, data['data'][v][2]]);
					}
				}

				if (!suggest.length) {
					//if nothing to suggest, check development status
			        	if (data['status']['development'] == 'Obsolete') {
						//report with style and text
						$('#row-'+ current['project'] +' .project-check').addClass ('level-security-update');
						$('#row-'+ current['project'] +' .row-info'). html ('Project development is stopped.');

						//report module is to be updated
						var toupdateSpan = $('#module-checker-toupdate');
						toupdateSpan.attr ('cnt', 1 + parseInt (toupdateSpan.attr ('cnt')));
						toupdateSpan.html (toupdateSpan.attr ('cnt'));

						//continue
						return _mc_project_has_updates (divMain, ind + 1);
					} 
					//something hand-made installed
					else if (!current['exists']) {
						//report with style and text
						$('#row-'+ current['project'] +' .project-check').addClass ('level-security-update');
						$('#row-'+ current['project'] +' .row-info'). html ('Project not found (usially it\'s custom developed module).');

						//report module is to be updated
						var toupdateSpan = $('#module-checker-toupdate');
						toupdateSpan.attr ('cnt', 1 + parseInt (toupdateSpan.attr ('cnt')));
						toupdateSpan.html (toupdateSpan.attr ('cnt'));

						//continue
						return _mc_project_has_updates (divMain, ind + 1);
					}

					//reflect to status message
					$('#row-'+ current['project']).hide ();
					var uptodateSpan = $('#module-checker-uptodate');
					uptodateSpan.attr ('cnt', 1 + parseInt (uptodateSpan.attr ('cnt')));
					uptodateSpan.html (uptodateSpan.attr ('cnt'));
				} else {
					strReleases = '';
					var level, levelFinal  = 0;
					var levels = ['level-none', 'level-new-features', 'level-bug-fix', 'level-security-update'];

					//display available updates
					for (var si in suggest) {
						strReleases += suggest[si][0] +' ('+ suggest[si][1] +') <a href="'+ suggest[si][3] +'">Download</a><br />';
						if (suggest[si][1] == 'Bug fixes') {
							level = 2;
						} else if (suggest[si][1] == 'Security update') {
							level = 3;
						} else if (suggest[si][1] == 'New features') {
							level = 1;
						}
					}

					if (level > levelFinal) {
						levelFinal = level;
					}
					//reflect importance of update
					$('#row-'+ current['project'] +' .project-check').addClass (levels[levelFinal]);

					//how display suggestions 
					var projSpan = $('.project-check[proj='+ current['project'] +']');					
					projSpan.removeClass ('mod-checking').html (projSpan.html () +' '+ current['version']);
					$('#row-'+ current['project'] +' .row-info'). html (strReleases);

					//update status message
					var toupdateSpan = $('#module-checker-toupdate');
					toupdateSpan.attr ('cnt', 1 + parseInt (toupdateSpan.attr ('cnt')));
					toupdateSpan.html (toupdateSpan.attr ('cnt'));
				}

				//continue
				return _mc_project_has_updates (divMain, ind + 1);
			}
			return;
		}
	);

	return;
}