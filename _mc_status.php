<?php
/**
 * @file
 *  File contains functions for collecting project info, fetched from drupal.org
 *  
 * Maintainer: Vladimir Reznichenko (kalessil)
 */

/**
 * Fetches projects' drupal.org URL.
 */
function _mc_build_info_url ($project, $branch = '6.x') {
	global $base_url;

	$site_key = md5 ($base_url . drupal_get_private_key ());
	return 'http://updates.drupal.org/release-history/'. $project .'/'. $branch .'?site_key='. $site_key .'&version=';
}

/**
 * Sends JSON with requested project information: development status, available updates.
 */
function _mc_status () {
	$data = array ();
	$result = 'ok';
	$strError = null;

	$project = $_REQUEST['project'];

	$url  = _mc_build_info_url ($project);
	$info = @file_get_contents ($url);

	if (empty ($info)) {
		$result   = 'err';
		$strError = t ('Getting project information failed.');

		drupal_json (array ('result' => $result, 'message' => $strError, 'data' => $data));
		exit ();
	}

	$dom = new domDocument ();
	$dom->loadXML ($info);

	$xpath = new DOMXPath ($dom);

	$devStatus = array ('maintenance' => '', 'development' => '');
	$devTerms  = $xpath->query ('//project//terms[1]/term');
	foreach ($devTerms as $dt) {
		foreach ($dt->getElementsByTagName ('name') as $dtn) {
			if ($dtn->nodeValue == 'Development status') {
				$devStatus['development'] = $dt->getElementsByTagName ('value')->item (0)->nodeValue;
			} else if ($dtn->nodeValue == 'Maintenance status') {
				$devStatus['maintenance'] = $dt->getElementsByTagName ('value')->item (0)->nodeValue;
			}
		}
	}

	$releases = $xpath->query ('//project/releases/release');
	foreach ($releases as $r) {
		$ve = $r->getElementsByTagName ('version_extra');
		if ($ve->length /*&& $ve->item (0)->nodeValue == 'dev' beta alfa rc*/) {
			continue;
		}

		$version = $r->getElementsByTagName ('version')->item (0)->nodeValue;
		$date    = $r->getElementsByTagName ('date')->item (0)->nodeValue;

		$changes = '';
		foreach ($r->getElementsByTagName ('term') as $term) {
			$changes = $term->getElementsByTagName ('value')->item (0)->nodeValue;
		}

		$downloadLink = $r->getElementsByTagName ('download_link')->item (0)->nodeValue;

		$data [$version] = array ($date, $changes, $downloadLink);
	}

	$info = db_fetch_array (db_query ('SELECT filename, info FROM {system} WHERE type="module" AND name="%s"', $_REQUEST['module']));
	$file = explode ('.', $info['filename']);
	unset ($file[count ($file) - 1]);
	$file = $_SERVER['DOCUMENT_ROOT']. implode ('.', $file) .'.info';

	$info = $info['info'];
	$info = unserialize ($info);

	$data['current'] = array ('date' => $info['datestamp'], 'version' => $info['version'], 'project' => $project, 'exists' => ($releases->length > 0));
	//update 'current' with info file content, if we found it
	if (file_exists ($file)) {
		$file = drupal_parse_info_file ($file);

		$data['current']['date']    = $file['datestamp']; 
		$data['current']['version'] = $file['version']; 
	}

	drupal_json (array ('result' => $result, 'message' => $strError, 'data' => $data, 'status' => $devStatus));
	exit ();
}

?>