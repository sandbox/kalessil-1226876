<?php
/**
 * @file
 *  File contains functions for collecting project info, fetched from drupal.org
 *  
 * Maintainer: Vladimir Reznichenko (kalessil)
 */

/**
 * Sends JSON with projects-modules information: list.
 */
function _mc_list_projects() {
	$listProjects = array ();

	$result = db_query ('SELECT filename, name, info FROM {system} WHERE type="module" AND filename LIKE "sites/%"');
	while ($mod = db_fetch_array ($result)) {
		//check if file exists: sometimes system table is not clean, when components non-properly removed 
		if (!file_exists ($mod['filename'])) {
			continue;
		}

		$info = unserialize ($mod['info']);
		if (empty ($info['project'])) {
			//may be info file not fully correct
			if (!empty ($info['name'])) {
				$info['project'] = $info['name'];
			} else {
				continue;
			}
		}

		$listProjects [$info['project']] []= $mod['name'];	
	}

	drupal_json ($listProjects);
	exit ();
}

?>