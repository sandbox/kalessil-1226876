<?php
/**
 * @file
 *  File contains page generation function.
 *  
 * Maintainer: Vladimir Reznichenko (kalessil)
 */

/**
 * Generates page output, adds required JS and CSS files.
 */
function _mc_page () {
	drupal_add_js  (drupal_get_path ('module', 'module_checker') . '/module_checker.js');
	drupal_add_css (drupal_get_path ('module', 'module_checker') . '/module_checker.css');

	return '<div id="module-checker-head"></div><div id="module-checker-main"></div><div id="module-checker-foot"></div>';
}

?>